//
//  RegistrationResponse.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 17.06.2024.
//

import Foundation

struct RegistrationResponse: Decodable {
    let token: String
}
