import SwiftUI
import Combine

struct LoginResponse: Decodable {
    let token: String
}

class LoginViewModel: ObservableObject {
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var rememberMe: Bool = false
    @Published var shouldNavigate: Bool = false
    @Published var errorMessage: String?

    private var cancellables = Set<AnyCancellable>()
    
    func login() {
        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/auth/login") else {
            self.errorMessage = "Invalid URL"
            print("Invalid URL")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let body: [String: AnyHashable] = [
            "email": email,
            "password": password
        ]

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
        } catch {
            self.errorMessage = "Failed to encode JSON"
            print("Failed to encode JSON: \(error)")
            return
        }

        // Log request body
        if let bodyData = request.httpBody, let bodyString = String(data: bodyData, encoding: .utf8) {
            print("Request Body: \(bodyString)")
        }

        URLSession.shared.dataTaskPublisher(for: request)
            .tryMap { output in
                guard let httpResponse = output.response as? HTTPURLResponse else {
                    throw URLError(.badServerResponse)
                }
                // Log response
                print("HTTP Status Code: \(httpResponse.statusCode)")
                if let responseData = String(data: output.data, encoding: .utf8) {
                    print("Server Response: \(responseData)")
                }
                if httpResponse.statusCode == 200 {
                    return output.data
                } else {
                    throw URLError(.init(rawValue: httpResponse.statusCode))
                }
            }
            .decode(type: LoginResponse.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    print("Login failed with error: \(error)")
                }
            }, receiveValue: { response in
                // Handle successful login
                DataManager.shared.updateAuthToken(response.token)
                self.shouldNavigate = true
            })
            .store(in: &cancellables)
    }
}
