import SwiftUI

struct DropDownPicker: View {
    @Binding var selection: Int?
    let options: [(Int, String)]

    var body: some View {
        Menu {
            ForEach(options, id: \.0) { option in
                Button(action: {
                    selection = option.0
                }) {
                    Text(option.1)
                }
            }
        } label: {
            Text(selection != nil ? options.first { $0.0 == selection }?.1 ?? "Выберите" : "Выберите")
                .foregroundColor(.black)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .padding()
                .background(Color.white)
                .cornerRadius(16)
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.gray, lineWidth: 1)
                )
        }
        .padding(.horizontal)
    }
}
