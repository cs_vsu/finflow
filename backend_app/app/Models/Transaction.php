<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'currencyId', 'walletId', 'categoryId', 'sum', 'type', 'description'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currencyId');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'walletId');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'categoryId');
    }
}
