import SwiftUI
import SwiftUICharts

struct HomeView: View {
    @EnvironmentObject var dataManager: DataManager
    @State private var selectedTab: String = "Сегодня"
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                LazyVStack(spacing: 20) {
                    TopNavigation()
                    balanceSection
                    AddButtons
                    ChartView()
                    DayPicker(selectedTab: $selectedTab)
//                    ForEach(filteredExpenses, id: \.id) { transaction in
//                        TransactionTab(transaction: transaction)
//
//                    }
//                    Spacer(minLength: 100)
                }
                .padding(.bottom, 100)
                .onAppear {
                    dataManager.fetchWallets()
                    dataManager.fetchBalance()
                }
                .background(
                    LinearGradient(gradient: Gradient(colors: [Color(red: 0.76, green: 1, blue: 0.91), .white]), startPoint: .top, endPoint: .bottom))
            }
            .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.all)
        }
    }
    
//    var filteredExpenses: [TransactionModel] {
//        let now = Date()
//        let calendar = Calendar.current
//        return dataManager.transactions.filter { transaction in
//            switch selectedTab {
//            case "Сегодня":
//                return calendar.isDateInToday(transaction.createdAt)
//            case "Неделя":
//                return calendar.isDate(transaction.createdAt, equalTo: now, toGranularity: .weekOfYear)
//            case "Месяц":
//                return calendar.isDate(transaction.createdAt, equalTo: now, toGranularity: .month)
//            case "Год":
//                return calendar.isDate(transaction.createdAt, equalTo: now, toGranularity: .year)
//            default:
//                return false
//            }
//        }
//    }
    
    var balanceSection: some View {
        VStack {
            Text("Ваш баланс")
                .font(.system(size: 20, weight: .medium))
                .lineSpacing(18)
                .foregroundColor(.black)
            
            Text("\(dataManager.balance, format: .currency(code: "RUB"))")
                .font(.system(size: 25, weight: .semibold))
                .foregroundColor(.black)
        }
        .frame(height: 85)
    }
    
    var AddButtons: some View {
        HStack(spacing: 20) {
            // Кнопка "Пополнения"
            ZStack {
                RoundedRectangle(cornerRadius: 28)
                    .fill(Color.button)
                    .frame(width: 164, height: 80)
                
                VStack(alignment: .center) {
                    Text("Пополнения")
                        .font(.system(size: 20, weight: .bold))
                        .foregroundColor(.white)
                    
                    if dataManager.transactions.filter({ $0.sum > 0 }).isEmpty {
                        Text("У вас нет пополнения")
                            .font(.system(size: 15, weight: .semibold))
                            .foregroundColor(.white)
                    } else {
                        Text("\(dataManager.transactions.filter({ $0.sum > 0 }).reduce(0) { $0 + $1.sum }, format: .currency(code: "RUB"))")
                            .font(.system(size: 25, weight: .semibold))
                            .foregroundColor(.white)
                    }
                }
                .padding(.leading, 16)
            }
            
            // Кнопка "Расходы"
            ZStack {
                RoundedRectangle(cornerRadius: 28)
                    .fill(Color.color5)
                    .frame(width: 164, height: 80)
                
                VStack(alignment: .center) {
                    Text("Расходы")
                        .font(.system(size: 20, weight: .bold))
                        .foregroundColor(.white)
                    
                    if dataManager.transactions.filter({ $0.sum < 0 }).isEmpty {
                        Text("У вас нет расходов")
                            .font(.system(size: 15, weight: .semibold))
                            .foregroundColor(.white)
                    } else {
                        Text("\(dataManager.transactions.filter({ $0.sum < 0 }).reduce(0) { $0 + $1.sum }, format: .currency(code: "RUB"))")
                            .font(.system(size: 25, weight: .semibold))
                            .foregroundColor(.white)
                    }
                }
                .padding(.leading, 16)
            }
        }
    }
}

struct ChartView: View {
    var body: some View {
        NavigationLink(destination: AnalysisView()) {
            VStack {
                HStack {
                    Spacer()
                    let chartStyle = ChartStyle(backgroundColor: Color.clear, accentColor: .button, secondGradientColor: .button, textColor: .black, legendTextColor: .black, dropShadowColor: .clear)
                    LineView(data: [8, 23, 54, 32, 12, 37, 7, 23, 43], legend: "Частота расходов", style: chartStyle)
                    Spacer()
                }
                Spacer()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

struct TransactionTab: View {
    var transaction: TransactionModel
    
    var body: some View {
        let categoryDetails = getCategoryDetails(for: transaction.categoryId) // Updated to use categoryId
        
        ZStack {
            RoundedRectangle(cornerRadius: 16)
                .foregroundColor(.clear)
                .frame(width: 344, height: 68)
                .background(categoryDetails.backgroundColor)
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.white, lineWidth: 1)
                )
            
            HStack(spacing: 16) {
                Image(systemName: categoryDetails.iconName)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 30, height: 30)
                    .padding(12)
                    .background(categoryDetails.backgroundColor)
                    .foregroundColor(categoryDetails.iconColor)
                    .cornerRadius(8)
                
                VStack(alignment: .leading, spacing: 4) {
                    Text("Category: \(transaction.categoryId)") // Updated to use categoryId
                        .font(.system(size: 16, weight: .medium))
                        .foregroundColor(.primary)
                    
                    Text(transaction.description!)
                        .font(.system(size: 14, weight: .regular))
                        .foregroundColor(.secondary)
                }
                
                Spacer()
                
                VStack(alignment: .trailing) {
                    Text(transaction.sum, format: .currency(code: "RUB"))
                        .font(.system(size: 16, weight: .bold))
                        .foregroundColor(categoryDetails.amountColor)
                        .font(.system(size: 14, weight: .regular))
                        .foregroundColor(.secondary)
                }
            }
            .padding(.horizontal, 16)
        }
        .frame(height: 80)
    }
    
    func getCategoryDetails(for categoryId: Int) -> (iconName: String, backgroundColor: Color, iconColor: Color, amountColor: Color) {
        switch categoryId {
        case 1:
            return ("cart.fill", Color(red: 0.99, green: 0.99, blue: 0.99), Color(red: 252/255, green: 172/255, blue: 18/255), Color.red)
        case 2:
            return ("bus.fill", Color(red: 0.99, green: 0.99, blue: 0.99), Color.blue, Color.purple)
        default:
            return ("questionmark.circle.fill", Color.gray, Color.black, Color.orange)
        }
    }
}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    return formatter
}()

struct DayPicker: View {
    @Binding var selectedTab: String
    
    var body: some View {
        HStack(spacing: 0) {
            DayPickerButton(text: "Сегодня", isSelected: selectedTab == "Сегодня") {
                selectedTab = "Сегодня"
            }
            DayPickerButton(text: "Неделя", isSelected: selectedTab == "Неделя") {
                selectedTab = "Неделя"
            }
            DayPickerButton(text: "Месяц", isSelected: selectedTab == "Месяц") {
                selectedTab = "Месяц"
            }
            DayPickerButton(text: "Год", isSelected: selectedTab == "Год") {
                selectedTab = "Год"
            }
        }
        .frame(width: 346, height: 34)
        .border(Color.white, width: 1)
        .cornerRadius(16)
        .background(Color.clear)
        .padding(.top, 330)
    }
}

struct DayPickerButton: View {
    let text: String
    let isSelected: Bool
    let action: () -> Void
    
    var body: some View {
        VStack {
            Button(action: action) {
                Text(text)
                    .font(.system(size: 14, weight: isSelected ? .bold : .regular))
                    .foregroundColor(isSelected ? Color(red: 60/255, green: 166/255, blue: 117/255) : Color(red: 145/255, green: 145/255, blue: 159/255))
                    .frame(maxWidth: .infinity)
                    .padding(.vertical, 8)
                    .background(isSelected ? Color(red: 194/255, green: 254/255, blue: 232/255) : Color.clear)
                    .cornerRadius(16)
            }
        }
    }
}

struct TopNavigation: View {
    var body: some View {
        HStack {
            Image("WomanPFP")
                .resizable()
                .frame(width: 32, height: 32)
                .cornerRadius(16)
                .shadow(color: Color(red: 0.68, green: 0, blue: 1, opacity: 1), radius: 3)
            
            Spacer()
            
            Button(action: {
                // Perform an action when this button is tapped
            }) {
                HStack {
                    Image(systemName: "calendar")
                        .foregroundColor(.button)
                        .padding(.trailing, 4)
                    Text("Октябрь")
                        .font(.system(size: 14, weight: .medium))
                        .lineLimit(1)
                        .foregroundColor(.button)
                }
                .padding(.horizontal, 16)
                .padding(.vertical, 8)
                .background(Color.white)
                .cornerRadius(40)
                .overlay(
                    RoundedRectangle(cornerRadius: 40)
                        .stroke(Color(red: 0.95, green: 0.95, blue: 0.98), lineWidth: 1)
                )
            }
            
            Spacer()
            
            Button(action: {
                // Perform an action when this button is tapped
            }) {
                Image(systemName: "bell")
                    .resizable()
                    .frame(width: 24, height: 24)
                    .foregroundColor(.button)
            }
        }
        .padding(.horizontal)
        .frame(height: 64)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().environmentObject(DataManager.shared)
    }
}
