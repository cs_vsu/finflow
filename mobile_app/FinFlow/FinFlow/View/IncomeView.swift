import SwiftUI

struct IncomeView: View {
    @EnvironmentObject var dataManager: DataManager
    @Environment(\.presentationMode) var presentationMode
    @State private var description: String = ""
    @State private var isRepeat: Bool = false
    @State private var categoryId: Int? = nil
    @State private var currencyId: Int? = nil
    @State private var walletId: Int? = nil // Добавлено
    @State private var amount: String = ""
    @State private var isEditingAmount: Bool = false

    var body: some View {
        ZStack {
            Color(red: 0.31, green: 0.65, blue: 0.33)
                .edgesIgnoringSafeArea(.all)

            VStack(spacing: 13) {
                Text("Пополнение")
                    .font(.system(size: 34, weight: .bold))
                    .foregroundColor(.white)

                HStack {
                    if isEditingAmount {
                        TextField("₽0", text: $amount)
                            .font(.system(size: 64, weight: .semibold))
                            .foregroundColor(Color.white)
                            .keyboardType(.decimalPad)
                            .frame(maxWidth: .infinity, alignment: .leading)
                    } else {
                        Text("₽\(amount.isEmpty ? "0" : amount)")
                            .font(.system(size: 64, weight: .semibold))
                            .foregroundColor(Color.white)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .onTapGesture {
                                isEditingAmount = true
                            }
                    }
                }

                Text("Сколько")
                    .font(.system(size: 18, weight: .semibold))
                    .foregroundColor(Color.white.opacity(0.64))
                    .frame(maxWidth: .infinity, alignment: .leading)

                VStack(spacing: 16) {
                    DropDownPicker(selection: $categoryId, options: dataManager.categories.map { ($0.key, $0.value) })
                    InputField(placeholder: "Описание", text: $description)
                    DropDownPicker(selection: $currencyId, options: dataManager.currencies.map { ($0.key, $0.value) })
                    DropDownPicker(selection: $walletId, options: dataManager.wallets.map { ($0.id, $0.name) }) // Добавлено

                    HStack {
                        VStack(alignment: .leading, spacing: 4) {
                            Text("Повтор")
                                .font(.system(size: 16, weight: .medium))
                                .foregroundColor(Color(red: 41 / 255, green: 43 / 255, blue: 45 / 255))
                            Text("Повторяющееся поступление")
                                .font(.system(size: 13, weight: .medium))
                                .foregroundColor(Color(red: 145 / 255, green: 145 / 255, blue: 159 / 255))
                        }
                        Spacer()
                        Toggle("", isOn: $isRepeat)
                            .toggleStyle(SwitchToggleStyle(tint: Color(red: 238 / 255, green: 229 / 255, blue: 255 / 255)))
                    }
                    .frame(height: 56)
                    .background(Color.white)
                    .cornerRadius(16)
                }
                .padding()
                .background(Color.white)
                .cornerRadius(20)

                Spacer()

                Button(action: {
                    addIncome()
                }) {
                    Text("Продолжить")
                        .font(.system(size: 18, weight: .semibold))
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                }
                .padding()
                .background(Color.green)
                .cornerRadius(16)
                .padding(.bottom, 32)
            }
            .padding(.horizontal, 24)
        }
        .onAppear {
            dataManager.fetchWallets()
            dataManager.fetchCategories()
            dataManager.fetchCurrencies()
        }
    }

    private func addIncome() {
        guard let categoryId = categoryId, let currencyId = currencyId, let walletId = walletId, let amountValue = Double(amount), let userToken = dataManager.authToken else { return }
        let income = TransactionModel(
            userToken: userToken,
            walletId: walletId,
            currencyId: currencyId,
            categoryId: categoryId,
            sum: amountValue,
            type: true,
            description: description
        )
        dataManager.addIncomeTransaction(transaction: income)
    }

    struct InputField: View {
        var placeholder: String
        @Binding var text: String

        var body: some View {
            TextField(placeholder, text: $text)
                .padding()
                .background(Color.white)
                .cornerRadius(16)
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.gray, lineWidth: 1)
                )
                .padding(.horizontal)
        }
    }
}

struct IncomeView_Previews: PreviewProvider {
    static var previews: some View {
        IncomeView().environmentObject(DataManager.shared)
    }
}
