import SwiftUI

struct BudgetView: View {
    @EnvironmentObject var dataManager: DataManager
    @State private var currentMonth = "Май"
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.white.edgesIgnoringSafeArea(.all)
                VStack {
                    if dataManager.wallets.isEmpty {
                        Spacer()
                        Image("BudgetPic")
                            .resizable()
                            .scaledToFit()
                        Text("Вы пока не составили бюджет\nДавайте создадим его")
                            .multilineTextAlignment(.center)
                            .padding()
                        Spacer()
                    } else {
                        List {
                            ForEach(dataManager.wallets) { wallet in
                                BudgetRow(wallet: wallet)
                            }
                            .onDelete(perform: deleteWallet)
                        }
                        .listStyle(PlainListStyle())
                    }
                    
                    NavigationLink(destination: CreateBudgetView()) {
                        Text("Создать бюджет")
                            .foregroundColor(.white)
                            .padding()
                            .frame(maxWidth: .infinity)
                            .background(Color.green)
                            .cornerRadius(10)
                    }
                    .padding(.horizontal)
                }
                .navigationBarHidden(true)
                .onAppear {
                    dataManager.fetchWallets()
                }
            }
        }
    }
    
    func deleteWallet(at offsets: IndexSet) {
        dataManager.deleteWallet(at: offsets)
    }
}

struct BudgetRow: View {
    var wallet: WalletModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            HStack {
                Text(wallet.name)
                    .font(.headline)
                Spacer()
                if wallet.balance > wallet.target {
                    Image(systemName: "exclamationmark.circle.fill")
                        .foregroundColor(.red)
                }
            }
            
            Text("Остаток ₽\(wallet.balance > 0 ? "\(wallet.balance, specifier: "%.2f")" : "0")")
                .font(.title3)
                .fontWeight(.bold)
                .foregroundColor(wallet.balance > 0 ? .black : .red)
            
            GeometryReader { geometry in
                ZStack(alignment: .leading) {
                    RoundedRectangle(cornerRadius: 5)
                        .fill(Color.gray.opacity(0.2))
                        .frame(height: 10)
                    RoundedRectangle(cornerRadius: 5)
                        .fill(wallet.balance > wallet.target ? Color.red : Color.blue)
                        .frame(width: CGFloat(wallet.balance / wallet.target) * geometry.size.width, height: 10)
                }
            }
            .frame(height: 10)
            
            HStack {
                Text("₽\(wallet.balance, specifier: "%.2f") из ₽\(wallet.target, specifier: "%.2f")")
                    .font(.caption)
                    .foregroundColor(.gray)
                Spacer()
            }
            
            if wallet.balance > wallet.target {
                Text("Вы превысили бюджет")
                    .font(.caption)
                    .foregroundColor(.red)
            }
        }
        .padding()
        .background(Color.white)
        .cornerRadius(10)
        .shadow(radius: 5)
        .padding(.horizontal)
    }
}

struct BudgetView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetView().environmentObject(DataManager.shared)
    }
}
