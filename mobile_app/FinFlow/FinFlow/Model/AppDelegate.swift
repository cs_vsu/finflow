//
//  AppDelegate.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 30.04.2024.
//

import Foundation
import UIKit
import AppMetricaCore

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        // Initializing the AppMetrica SDK.
        let configuration = AppMetricaConfiguration(apiKey: "32f10f08-a3f0-4298-90e1-4a6b2bfa45bf")
        AppMetrica.activate(with: configuration!)
        return true
        
    }
    
}
