# Приложение "FinFlow"

## Описание

Приложение является инновационным инструментом, который помогает управлять финансовыми ресурсами.

### Над проектом работают:

Студенты ФКН, 3 курса, 3 группы

- [Суворов Дмитрий](https://github.com/suvor03) - Product Manager, Team Lead, Backend разработчик
- [Ляшенко Егор](https://github.com/egrdze) - Project Manager, Frontend разработчик, дизайнер
- [Понятовская Ксения](https://github.com/ksushagumanoid) - Технический писатель, системный аналитик, тестировщик

## Сервисы

- [Trello](https://trello.com/b/xqfJ0rx0/finflow)
- [Miro](https://miro.com/app/board/uXjVNqAfVAI=/?share_link_id=765332935898)
- [Figma](https://goo.su/49i072)

## Документация

- Техническое задание: [(docx)](https://clck.ru/39QjuW) [(pdf)](https://clck.ru/39QkAF)
- Сопроводительное письмо: [(docx)](https://clck.ru/39QkJc) [(pdf)](https://clck.ru/39QkMa)
- Курсовой проект: [(docx)](https://clck.ru/3Awkmd) [(pdf)](https://clck.ru/3Awkn6)

## Гайд по разворачиванию приложения

- Клиентская часть: [(README)](https://clck.ru/3AwiNJ)
- Серверная часть: [(README)](https://clck.ru/3AwiPF)

## Презентация проекта

- Презентация ТЗ: [(pptx)](https://clck.ru/39QkTf) [(pdf)](https://clck.ru/39QkWw)
- Видеодемонстрация ТЗ: [YouTube](https://youtu.be/s_wm7wabh5c)


- Презентация курсового проекта: [(pptx)](https://clck.ru/3AwiMz) [(pdf)](https://clck.ru/3AwiMS)

- Видеодемонстрация курсового проекта: [YouTube](https://youtu.be/aouvLVh1o24)
- Видеодемонстрация клиентской части: [YouTube](https://www.youtube.com/watch?v=74KKwZjN-xc)
- Видеодемонстрация серверной части: [YouTube](https://youtu.be/EW2RdEHxzKg)

## Аналитика приложения

- Скриншоты: [png](https://gitlab.com/cs_vsu/finflow/-/tree/main/analytics?ref_type=heads)