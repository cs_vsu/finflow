import SwiftUI
import Combine

struct UserProfile: Decodable {
    let firstName: String
    let lastName: String
    let email: String
}

struct UserResponse: Decodable {
    let data: UserProfile
}

class ProfileViewModel: ObservableObject {
    @Published var firstName: String = "Ирина"
    @Published var lastName: String = "Кузнецова"
    @Published var email: String = "irina.kuznetsova@example.com"
    @Published var isLoggedOut: Bool = false

    private var cancellables = Set<AnyCancellable>()

    func fetchUserProfile() {
        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/auth/user") else {
            print("Invalid URL")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(UserDefaults.standard.string(forKey: "authToken") ?? "")", forHTTPHeaderField: "Authorization")

        URLSession.shared.dataTaskPublisher(for: request)
            .tryMap { output in
                guard let httpResponse = output.response as? HTTPURLResponse else {
                    throw URLError(.badServerResponse)
                }
                if httpResponse.statusCode == 200 {
                    return output.data
                } else {
                    if let responseData = String(data: output.data, encoding: .utf8) {
                        print("Server Error Response: \(responseData)")
                    }
                    throw URLError(.init(rawValue: httpResponse.statusCode))
                }
            }
            .decode(type: UserResponse.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("Fetch failed with error: \(error)")
                }
            }, receiveValue: { response in
                let userProfile = response.data
                self.firstName = userProfile.firstName
                self.lastName = userProfile.lastName
                self.email = userProfile.email
                print("Fetched user profile: \(userProfile)")
            })
            .store(in: &cancellables)
    }

    func updateUserProfile(newFirstName: String, newLastName: String, newEmail: String) {
        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/user/update") else {
            print("Invalid URL")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(UserDefaults.standard.string(forKey: "authToken") ?? "")", forHTTPHeaderField: "Authorization")

        let body: [String: AnyHashable] = [
            "firstName": newFirstName,
            "lastName": newLastName,
            "email": newEmail
        ]

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
        } catch {
            print("Failed to encode JSON: \(error)")
            return
        }

        // Логирование тела запроса
        if let bodyData = request.httpBody, let bodyString = String(data: bodyData, encoding: .utf8) {
            print("Request Body: \(bodyString)")
        }

        URLSession.shared.dataTaskPublisher(for: request)
            .tryMap { output in
                guard let httpResponse = output.response as? HTTPURLResponse else {
                    throw URLError(.badServerResponse)
                }
                // Логирование ответа сервера
                print("HTTP Status Code: \(httpResponse.statusCode)")
                if httpResponse.statusCode == 200 {
                    return output.data
                } else {
                    // Логирование ошибки сервера
                    if let responseData = String(data: output.data, encoding: .utf8) {
                        print("Server Error Response: \(responseData)")
                    }
                    throw URLError(.init(rawValue: httpResponse.statusCode))
                }
            }
            .decode(type: UserResponse.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    self.fetchUserProfile()  // Запрос актуальных данных после успешного обновления
                    break
                case .failure(let error):
                    print("Update failed with error: \(error)")
                }
            }, receiveValue: { response in
                let userProfile = response.data
                // Обновление данных после успешного запроса
                self.firstName = userProfile.firstName
                self.lastName = userProfile.lastName
                self.email = userProfile.email
                print("Updated user profile: \(userProfile)")
            })
            .store(in: &cancellables)
    }
    
    func logout() {
        // Очистка токена и обновление состояния
        UserDefaults.standard.removeObject(forKey: "authToken")
        isLoggedOut = true
    }
}
