<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $date = Carbon::now();
        DB::table('wallets')->insert([
            'userId' => 1,
            'name' => 'Основной',
            'balance' => 1000.00,
            'created_at' => $date,
            'updated_at' => $date
        ]);

        DB::table('wallets')->insert([
            'userId' => 2,
            'name' => 'На отпуск',
            'balance' => 50600,
            'created_at' => $date,
            'updated_at' => $date
        ]);
    }
}
