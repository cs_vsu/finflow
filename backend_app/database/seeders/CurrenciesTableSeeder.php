<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $date = Carbon::now();
        DB::table('currencies')->insert([
            ['name' => 'USD', 'value' => 70, 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'EUR', 'value' => 90.8, 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'GBP', 'value' => 25.5, 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'RUB', 'value' => 1, 'created_at' => $date, 'updated_at' => $date],
        ]);
    }
}
