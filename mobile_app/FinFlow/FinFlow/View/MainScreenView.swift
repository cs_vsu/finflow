
import SwiftUI

// Основной экран
struct MainScreenView: View {
    @State private var selectedTab: Int = 0
    @State private var showExtraButtons: Bool = false
    
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                MainContentView(selectedTab: $selectedTab)
                Spacer()
                CustomTabBar(selectedTab: $selectedTab, showExtraButtons: $showExtraButtons)
            }
            .background(
                LinearGradient(gradient: Gradient(colors: [Color(red: 0.76, green: 1, blue: 0.91), .white]), startPoint: .top, endPoint: .bottom)
            )
            .edgesIgnoringSafeArea(.all)
        }
        .navigationBarHidden(true)
    }
}


// Контент основного экрана
struct MainContentView: View {
    @EnvironmentObject var budgetData: BudgetData
    @Binding var selectedTab: Int

    var body: some View {
        switch selectedTab {
        case 0:
            HomeView().padding(.top, 60)
        case 1: 
            TransactionView()
        case 2:
            BudgetView().environmentObject(budgetData).padding(.top, 50)
        case 3:
            ProfileView().padding(.top, 80)
        default:
            Text("Ошибка: некорректная вкладка")
        }
    }
}

// Пользовательский TabBar
struct CustomTabBar: View {
    @Binding var selectedTab: Int
    @Binding var showExtraButtons: Bool

    var body: some View {
        HStack {
            Button(action: { self.selectedTab = 0 }) {
                Image(systemName: "house.fill")
                    .foregroundColor(.button)
            }
            Spacer()
            Button(action: { self.selectedTab = 1 }) {
                Image(systemName: "arrow.left.arrow.right")
                    .foregroundColor(.button)
            }
            Spacer()
            // Кнопка плюс с анимацией
            Button(action: {
                withAnimation {
                    self.showExtraButtons.toggle()
                }
            }) {
                Image(systemName: "plus.circle.fill")
                    .font(.largeTitle)
                    .foregroundColor(.button)
            }
            Spacer()
            Button(action: { self.selectedTab = 2 }) {
                Image(systemName: "creditcard.fill")
                    .foregroundColor(.button)
            }
            Spacer()
            Button(action: { self.selectedTab = 3 }) {
                Image(systemName: "person.crop.circle.fill")
                    .foregroundColor(.button)
                
            }
        }
        .padding()
        .background(Color.white)
        .clipShape(Capsule())
        .shadow(radius: 6)
        .overlay(
            ExtraButtonsView(show: $showExtraButtons)
                .offset(y: showExtraButtons ? -70 : 100)
        )
    }
}

// Дополнительные кнопки, которые появляются при нажатии на плюс
struct ExtraButtonsView: View {
    @Binding var show: Bool

    var body: some View {
        HStack {
            if show {
                NavigationLink(destination: ExpenseView()) {
                    Image(systemName: "minus.square.fill")
                        .foregroundColor(.red)
                }
                .buttonStyle(CircularButtonStyle())
                .navigationBarHidden(true)

                NavigationLink(destination: IncomeView()) {
                    Image(systemName: "plus.app.fill")
                        .foregroundColor(.button)
                }
                .buttonStyle(CircularButtonStyle())
            }
        }
        .transition(.scale)
    }
}

// Стиль кнопки для круглых кнопок
struct CircularButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color.color1)
            .clipShape(Circle())
            .foregroundColor(.white)
            .rotationEffect(configuration.isPressed ? .degrees(90) : .zero)
            .scaleEffect(configuration.isPressed ? 1.2 : 1.0)
            .animation(.easeOut(duration: 0.2), value: configuration.isPressed)
    }
}

// Предпросмотр
struct MainScreenView_Previews: PreviewProvider {
    static var previews: some View {
        MainScreenView()
            .environmentObject(DataManager.shared)
            .environmentObject(BudgetData())
            .environmentObject(ProfileViewModel())
    }
}
