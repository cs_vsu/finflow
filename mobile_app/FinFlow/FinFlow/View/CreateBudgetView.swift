import SwiftUI
import Combine

struct CreateBudgetView: View {
    @EnvironmentObject var dataManager: DataManager
    @State private var showAlerts = false
    @State private var budgetAmount: String = ""
    @State private var alertPercentage: Double = 80
    @State private var categoryInput: String = ""
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            Color(red: 58 / 255, green: 217 / 255, blue: 127 / 255)
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                HStack {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }) {
                        Image(systemName: "chevron.left")
                            .foregroundColor(.white)
                            .padding()
                            .background(Color.green)
                            .cornerRadius(10)
                    }
                    Spacer()
                    Text("Создать бюджет")
                        .font(.headline)
                        .foregroundColor(.white)
                    Spacer()
                    Color.clear.frame(width: 48, height: 48)
                }
                .padding([.top, .horizontal])
                .navigationBarHidden(true)
                
                Spacer()
                
                VStack(spacing: 16) {
                    Text("Сколько вы хотите потратить?")
                        .font(.title3)
                        .foregroundColor(.white.opacity(0.7))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, 32)
                    
                    HapticButtonView(
                        isAlertOn: $showAlerts,
                        alertPercentage: $alertPercentage,
                        categoryInput: $categoryInput,
                        budgetAmount: $budgetAmount
                    )

                    Button("Продолжить") {
                        if let budgetAmountDouble = Double(budgetAmount), !categoryInput.isEmpty {
                            dataManager.createWallet(name: categoryInput, target: budgetAmountDouble)
                            dataManager.fetchWallets() // Refresh wallets to get the updated list
                            presentationMode.wrappedValue.dismiss() // Navigate back to BudgetView
                        }
                    }
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.green)
                    .cornerRadius(16)
                    .padding(.horizontal)
                }
                .padding(.bottom, 20)
                
                Spacer()
            }
        }
    }
}

struct HapticButtonView: View {
    @Binding var isAlertOn: Bool
    @Binding var alertPercentage: Double
    @Binding var categoryInput: String
    @Binding var budgetAmount: String

    var body: some View {
        VStack(spacing: 16) {
            TextField("Введите категорию", text: $categoryInput)
                .padding()
                .background(Color.white)
                .cornerRadius(16)
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.gray, lineWidth: 1)
                )
                .padding(.horizontal)
            
            TextField("Введите сумму бюджета", text: $budgetAmount)
                .keyboardType(.decimalPad)
                .padding()
                .background(Color.white)
                .cornerRadius(16)
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.gray, lineWidth: 1)
                )
                .padding(.horizontal)
            
            VStack {
                Toggle("Получать предупреждения", isOn: $isAlertOn)
                    .toggleStyle(SwitchToggleStyle(tint: .green))
                
                if isAlertOn {
                    Slider(value: $alertPercentage, in: 0...100, step: 1)
                        .accentColor(.green)
                    HStack {
                        Spacer()
                        Text("\(Int(alertPercentage))%")
                            .foregroundColor(.green)
                            .padding(.trailing)
                    }
                }
            }
            .padding([.horizontal, .bottom])
        }
        .padding(.top)
        .background(Color.white)
        .cornerRadius(25)
        .frame(height: isAlertOn ? 230 : 190)
    }
}

struct CreateBudgetView_Previews: PreviewProvider {
    static var previews: some View {
        CreateBudgetView().environmentObject(DataManager.shared)
    }
}
