<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FinFlow</title>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
</head>
<body>
<div class="container">
    <div class="header">
        <img src="{{ asset('img/Logo.png') }}" alt="FinFlow Logo">
        <div class="title">FinFlow</div>
        <div class="subtitle">Мобильное приложение</div>
        <div class="subtitle">Подготовили студенты 3 курса 3 группы ФКН</div>
    </div>
    <div class="footer">
        Разработчики проекта: Суворов Дмитрий, Ляшенко Егор, Понятовская Ксения
    </div>
</div>
</body>
</html>
