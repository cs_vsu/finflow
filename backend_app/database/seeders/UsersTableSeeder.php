<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $date = Carbon::now();
        DB::table('users')->insert([
            'firstName' => 'Иван',
            'lastName' => 'Иванов',
            'email' => 'ivan@gmail.com',
            'password' => Hash::make('123'),
            'isPremium' => false,
            'created_at' => $date,
            'updated_at' => $date
        ]);

        DB::table('users')->insert([
            'firstName' => 'Петр',
            'lastName' => 'Петров',
            'email' => 'pet@yandex.ru',
            'password' => Hash::make('123'),
            'isPremium' => true,
            'created_at' => $date,
            'updated_at' => $date
        ]);
    }
}
