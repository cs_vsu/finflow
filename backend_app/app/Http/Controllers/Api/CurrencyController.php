<?php

namespace App\Http\Controllers\Api;

use App\Models\Currency;

class CurrencyController
{
    public function getCurrencies()
    {
        $currencies = Currency::all(['name', 'value']);

        $formattedCurrencies = $currencies->map(function ($currency) {
            return ['name' => $currency->name, 'value' => $currency->value];
        });

        return response()->json(['data' => $formattedCurrencies], 200);
    }
}
