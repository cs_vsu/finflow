<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Wallet;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function getBalance(Request $request)
    {
        $user = $request->user();

        $wallets = Wallet::where('userId', $user->id)->get();

        if ($wallets->isEmpty()) {
            return response()->json([
                'message' => 'No wallets found for the authenticated user.'
            ], 404);
        }

        $totalBalance = $wallets->sum('balance');

        return response()->json([
            'userId' => $user->id,
            'totalBalance' => $totalBalance
        ], 200);
    }

    public function createWallet(Request $request)
    {
        $user = $request->user();

        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'target' => 'numeric|nullable'
        ]);

        $wallet = new Wallet;
        $wallet->userId = $user->id;
        $wallet->name = $validated['name'];
        $wallet->target = $validated['target'] ?? 0;
        $wallet->save();

        return response()->json([
            'message' => 'Wallet created successfully.',
            'wallet' => $wallet
        ], 201);
    }

    public function getAllWallets(Request $request)
    {
        $user = $request->user();

        $wallets = Wallet::where('userId', $user->id)->get(['name', 'balance', 'target']);

        if ($wallets->isEmpty()) {
            return response()->json([
                'message' => 'No wallets found for the authenticated user.'
            ], 404);
        }

        $walletData = $wallets->map(function ($wallet) {
            return [
                'name' => $wallet->name,
                'balance' => $wallet->balance,
                'target' => $wallet->target
            ];
        });

        return response()->json([
            'userId' => $user->id,
            'wallets' => $walletData
        ], 200);
    }

    public function deleteWallet(Request $request, $walletId)
    {
        $user = $request->user();

        $wallet = Wallet::where('userId', $user->id)->where('id', $walletId)->first();

        if (!$wallet) {
            return response()->json([
                'message' => 'Wallet not found or not owned by the user.'
            ], 404);
        }

        $wallet->delete();

        return response()->json([
            'message' => 'Wallet deleted successfully.'
        ], 200);
    }

    public function updateWallet(Request $request, $walletId)
    {
        $user = $request->user();

        $wallet = Wallet::where('userId', $user->id)->where('id', $walletId)->first();

        if (!$wallet) {
            return response()->json([
                'message' => 'Wallet not found or not owned by the user.'
            ], 404);
        }

        $validated = $request->validate([
            'name' => 'string|max:255',
            'balance' => 'numeric',
            'target' => 'numeric|nullable'
        ]);

        if ($request->has('name')) {
            $wallet->name = $validated['name'];
        }
        if ($request->has('balance')) {
            $wallet->balance = $validated['balance'];
        }
        if ($request->has('target')) {
            $wallet->target = $validated['target'];
        }
        $wallet->save();

        return response()->json([
            'message' => 'Wallet updated successfully.',
            'wallet' => $wallet
        ], 200);
    }
}
