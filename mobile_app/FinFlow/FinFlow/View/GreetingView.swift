//
//  GreetingView.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 20.03.2024.
//

import SwiftUI

struct GreetingView: View {
    var body: some View {
        VStack {
            Spacer(minLength: 126)
            
            Image("GreetingScreen")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 391, height: 396)
            
            
            Spacer(minLength: 34)
            
            Text("Храните и берегите")
                .font(.system(size: 29, weight: .bold))
                .foregroundColor(.black)
            
            Spacer(minLength: 34)
            
            Text("Считайте и распределяйте свои ")
                .font(.system(size: 20, weight: .medium))
                .foregroundColor(.black)
            Text("расходы")
                .font(.system(size: 20, weight: .medium))
                .foregroundColor(Color(red: 60/255, green: 166/255, blue: 117/255)) +
            Text(" правильно вместе с нами!")
                .font(.system(size: 20, weight: .medium))
                .foregroundColor(.black)
            
            Spacer(minLength: 148)
            
            Button(action: {}) {
                HStack {
                    NavigationLink(destination: MainScreenView()) {
                        Spacer()
                        Text("Продолжить")
                            .font(.system(size: 20, weight: .semibold))
                            .foregroundColor(.white)
                        Spacer()
                        Image(systemName: "arrow.right")
                            .foregroundColor(.white)
                    }
                }
                .padding()
                .background(Color(red: 60/255, green: 166/255, blue: 117/255))
                .cornerRadius(10)
            }
            .frame(width: 300, height: 50)
            
            Spacer(minLength: 126)
            
            RoundedRectangle(cornerRadius: 100)
                .fill(.black)
                .frame(width: 134, height: 5)
                .padding(.bottom, 9)
        }
        .background(.white)
        .edgesIgnoringSafeArea(.all)
    }
}


#Preview {
    GreetingView()
}
