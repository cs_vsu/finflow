<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $date = Carbon::now();
        DB::table('transactions')->insert([
            'currencyId' => 1,
            'walletId' => 1,
            'categoryId' => 1,
            'sum' => 100.00,
            'type' => true,
            'description' => 'день рождения',
            'created_at' => $date,
            'updated_at' => $date
        ]);

        DB::table('transactions')->insert([
            'currencyId' => 2,
            'walletId' => 2,
            'categoryId' => 2,
            'sum' => 50.00,
            'type' => false,
            'description' => 'Bus ticket',
            'created_at' => $date,
            'updated_at' => $date
        ]);
    }
}
