import Foundation
import Combine

struct WalletModel: Identifiable, Codable {
    
    var id: Int
    var name: String
    var balance: Double
    var target: Double
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case balance
        case target
    }
}

struct TransactionModel: Identifiable, Codable {
    var id: Int?
    var userToken: String
    var walletId: Int
    var currencyId: Int
    var categoryId: Int
    var sum: Double
    var type: Bool
    var description: String?
    
    enum CodingKeys: String, CodingKey {
        case userToken = "user_token"
        case walletId
        case currencyId
        case categoryId
        case sum
        case type
        case description
    }
}

struct WalletsResponse: Codable {
    var userId: Int
    var wallets: [WalletModel]
    
    enum CodingKeys: String, CodingKey {
        case userId
        case wallets
    }
}

struct BalanceResponse: Codable {
    var userId: Int
    var totalBalance: Double
    
    enum CodingKeys: String, CodingKey {
        case userId
        case totalBalance
        
    }
}

struct ErrorResponse: Codable {
    var message: String
}

struct CategoriesResponse: Codable {
    var data: [String]
}

struct CurrenciesResponse: Codable {
    var data: [Currency]
    
    struct Currency: Codable {
        var name: String
        var value: String
    }
}

class DataManager: ObservableObject {
    @Published var categories: [Int: String] = [:]
    @Published var currencies: [Int: String] = [:]
    @Published var budgets: [WalletModel] = []
    @Published var transactions: [TransactionModel] = []
    @Published var wallets: [WalletModel] = []
    @Published var balance: Double = 0.0
    @Published var isAuthenticated: Bool = false
    @Published var errorMessage: String?
    
    static let shared = DataManager()
    
    var cancellables = Set<AnyCancellable>()
    var authToken: String? {
        didSet {
            if let token = authToken {
                UserDefaults.standard.set(token, forKey: "authToken")
            } else {
                UserDefaults.standard.removeObject(forKey: "authToken")
            }
        }
    }
    
    private init() {
        self.authToken = UserDefaults.standard.string(forKey: "authToken")
        self.isAuthenticated = self.authToken != nil
    }
    
    func updateAuthToken(_ token: String) {
        self.authToken = token
        self.isAuthenticated = true
        self.clearData()
        fetchWallets()
        fetchCategories()
        fetchCurrencies()
    }
    
    func createWallet(name: String, target: Double) {
        guard isAuthenticated else {
            print("Not authenticated. Please provide a valid token.")
            return
        }

        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/wallet/create") else {
            print("Invalid URL")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let token = authToken {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        } else {
            print("No auth token found")
            return
        }

        let wallet = WalletModel(id: 0, name: name, balance: 0, target: target)
        
        do {
            let jsonData = try JSONEncoder().encode(wallet)
            request.httpBody = jsonData

            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print("JSON Data being sent: \(jsonString)")
            }
        } catch {
            print("Failed to serialize JSON for wallet: \(error.localizedDescription)")
            return
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = "Error: \(error.localizedDescription)"
                }
                print("Error: \(error.localizedDescription)")
                return
            }

            if let httpResponse = response as? HTTPURLResponse {
                print("HTTP Response Code: \(httpResponse.statusCode)")

                guard let data = data else {
                    print("No data received.")
                    return
                }

                if let responseData = String(data: data, encoding: .utf8) {
                    print("Server Response: \(responseData)")
                }

                if httpResponse.statusCode == 200 {
                    do {
                        let newWallet = try JSONDecoder().decode(WalletModel.self, from: data)
                        DispatchQueue.main.async {
                            self.wallets.append(newWallet)
                            self.updateBalance()
                            print("Wallet successfully created: \(newWallet)")
                        }
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                } else {
                    do {
                        let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: data)
                        DispatchQueue.main.async {
                            self.errorMessage = errorResponse.message
                        }
                        print("Error Response: \(errorResponse.message)")
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                }
            }
        }
        task.resume()
    }
    
    func fetchWallets() {
        guard isAuthenticated else {
            print("Not authenticated. Please provide a valid token.")
            return
        }

        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/wallets") else {
            print("Invalid URL")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        if let token = authToken {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        } else {
            print("No auth token found")
            return
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = "Error: \(error.localizedDescription)"
                }
                print("Error: \(error.localizedDescription)")
                return
            }

            if let httpResponse = response as? HTTPURLResponse {
                print("HTTP Response Code: \(httpResponse.statusCode)")

                guard let data = data else {
                    print("No data received.")
                    return
                }

                if let responseData = String(data: data, encoding: .utf8) {
                    print("Server Response: \(responseData)")
                }

                if httpResponse.statusCode == 200 {
                    do {
                        let walletsResponse = try JSONDecoder().decode(WalletsResponse.self, from: data)
                        DispatchQueue.main.async {
                            self.wallets = walletsResponse.wallets
                            self.updateBalance()
                            print("Fetched wallets: \(walletsResponse.wallets)")
                        }
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                } else {
                    do {
                        let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: data)
                        DispatchQueue.main.async {
                            self.errorMessage = errorResponse.message
                        }
                        print("Error Response: \(errorResponse.message)")
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                }
            }
        }
        task.resume()
    }
    func fetchBalance() {
        guard isAuthenticated else {
            print("Not authenticated. Please provide a valid token.")
            return
        }
        
        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/wallet/balance") else {
            print("Invalid URL")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        if let token = authToken {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        } else {
            print("No auth token found")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = "Error: \(error.localizedDescription)"
                }
                print("Error: \(error.localizedDescription)")
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                print("HTTP Response Code: \(httpResponse.statusCode)")
                
                guard let data = data else {
                    print("No data received.")
                    return
                }
                
                if let responseData = String(data: data, encoding: .utf8) {
                    print("Server Response: \(responseData)")
                }
                
                if httpResponse.statusCode == 200 {
                    do {
                        let balanceResponse = try JSONDecoder().decode(BalanceResponse.self, from: data)
                        DispatchQueue.main.async {
                            self.balance = balanceResponse.totalBalance
                            print("Fetched balance: \(self.balance)")
                        }
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                } else {
                    do {
                        let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: data)
                        DispatchQueue.main.async {
                            self.errorMessage = errorResponse.message
                        }
                        print("Error Response: \(errorResponse.message)")
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                }
            }
        }
        task.resume()
    }
    
    func deleteWallet(at offsets: IndexSet) {
        guard isAuthenticated else {
            print("Not authenticated. Please provide a valid token.")
            return
        }

        offsets.forEach { index in
            let walletToDelete = wallets[index]
            
            guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/wallets/\(walletToDelete.id)") else {
                print("Invalid URL")
                return
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = "DELETE"
            if let token = authToken {
                request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            } else {
                print("No auth token found")
                return
            }
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    DispatchQueue.main.async {
                        self.errorMessage = "Error: \(error.localizedDescription)"
                    }
                    print("Error: \(error.localizedDescription)")
                    return
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    print("HTTP Response Code: \(httpResponse.statusCode)")
                    
                    if httpResponse.statusCode == 200 {
                        DispatchQueue.main.async {
                            self.wallets.remove(at: index)
                            self.updateBalance()
                            print("Wallet successfully deleted")
                        }
                    } else {
                        guard let data = data else {
                            print("No data received.")
                            return
                        }
                        
                        do {
                            let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: data)
                            DispatchQueue.main.async {
                                self.errorMessage = errorResponse.message
                            }
                            print("Error Response: \(errorResponse.message)")
                        } catch {
                            DispatchQueue.main.async {
                                self.errorMessage = "Decoding error: \(error.localizedDescription)"
                            }
                            print("Decoding error: \(error.localizedDescription)")
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    func fetchCategories() {
        guard isAuthenticated else {
            print("Not authenticated. Please provide a valid token.")
            return
        }
        
        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/categories") else {
            print("Invalid URL")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        if let token = authToken {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        } else {
            print("No auth token found")
            return
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = "Error: \(error.localizedDescription)"
                }
                print("Error: \(error.localizedDescription)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                print("Failed to fetch categories")
                return
            }
            
            guard let data = data else {
                print("No data received.")
                return
            }
            
            do {
                let categoriesResponse = try JSONDecoder().decode(CategoriesResponse.self, from: data)
                DispatchQueue.main.async {
                    self.categories = categoriesResponse.data.enumerated().reduce(into: [Int: String]()) { (result, item) in
                        result[item.offset + 1] = item.element // Adjust index to start from 1
                    }
                    print("Fetched categories: \(self.categories)")
                }
            } catch {
                DispatchQueue.main.async {
                    self.errorMessage = "Decoding error: \(error.localizedDescription)"
                }
                print("Decoding error: \(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    func fetchCurrencies() {
        guard isAuthenticated else {
            print("Not authenticated. Please provide a valid token.")
            return
        }
        
        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/currencies") else {
            print("Invalid URL")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        if let token = authToken {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        } else {
            print("No auth token found")
            return
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = "Error: \(error.localizedDescription)"
                }
                print("Error: \(error.localizedDescription)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                print("Failed to fetch currencies")
                return
            }
            
            guard let data = data else {
                print("No data received.")
                return
            }
            
            do {
                let currenciesResponse = try JSONDecoder().decode(CurrenciesResponse.self, from: data)
                DispatchQueue.main.async {
                    self.currencies = currenciesResponse.data.enumerated().reduce(into: [Int: String]()) { (result, item) in
                        result[item.offset + 1] = item.element.name // Adjust index to start from 1
                    }
                    print("Fetched currencies: \(self.currencies)")
                }
            } catch {
                DispatchQueue.main.async {
                    self.errorMessage = "Decoding error: \(error.localizedDescription)"
                }
                print("Decoding error: \(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    func addIncomeTransaction(transaction: TransactionModel) {
        addTransaction(transaction: transaction, endpoint: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/transaction/income")
    }

    func addExpenseTransaction(transaction: TransactionModel) {
        addTransaction(transaction: transaction, endpoint: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/transaction/expense")
    }

   
    private func addTransaction(transaction: TransactionModel, endpoint: String) {
        guard isAuthenticated else {
            print("Not authenticated. Please provide a valid token.")
            return
        }
        
        guard let url = URL(string: endpoint) else {
            print("Invalid URL")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let token = authToken {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        } else {
            print("No auth token found")
            return
        }
        
        do {
            let encoder = JSONEncoder()
            var transactionCopy = transaction
            transactionCopy.id = nil // Убираем id перед кодированием
            let jsonData = try encoder.encode(transactionCopy)
            request.httpBody = jsonData
            
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print("JSON Data being sent: \(jsonString)")
            }
        } catch {
            print("Failed to serialize JSON for transaction: \(error.localizedDescription)")
            return
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = "Error: \(error.localizedDescription)"
                }
                print("Error: \(error.localizedDescription)")
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                print("HTTP Response Code: \(httpResponse.statusCode)")
                
                guard let data = data else {
                    print("No data received.")
                    return
                }
                
                if let responseData = String(data: data, encoding: .utf8) {
                    print("Server Response: \(responseData)")
                }
                
                if httpResponse.statusCode == 200 {
                    do {
                        let responseDict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                        let transactionData = responseDict?["transaction"] as? [String: Any]
                        
                        let jsonData = try JSONSerialization.data(withJSONObject: transactionData as Any)
                        let addedTransaction = try JSONDecoder().decode(TransactionModel.self, from: jsonData)
                        DispatchQueue.main.async {
                            self.transactions.append(addedTransaction)
                            self.updateBalance()
                            print("Transaction successfully added: \(addedTransaction)")
                        }
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                } else {
                    do {
                        let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: data)
                        DispatchQueue.main.async {
                            self.errorMessage = errorResponse.message
                        }
                        print("Error Response: \(errorResponse.message)")
                    } catch {
                        DispatchQueue.main.async {
                            self.errorMessage = "Decoding error: \(error.localizedDescription)"
                        }
                        print("Decoding error: \(error.localizedDescription)")
                    }
                }
            }
        }
        task.resume()
    }

    private func updateBalance() {
        self.balance = wallets.reduce(0) { $0 + $1.balance }
    }
    
    func logout() {
        self.authToken = nil
        self.isAuthenticated = false
        self.clearData()
        print("User logged out successfully")
    }
    
    private func clearData() {
        self.categories.removeAll()
        self.currencies.removeAll()
        self.budgets.removeAll()
        self.transactions.removeAll()
        self.wallets.removeAll()
        self.balance = 0.0
        self.errorMessage = nil
    }
}
