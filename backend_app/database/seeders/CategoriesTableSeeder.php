<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $date = Carbon::now();
        DB::table('categories')->insert([
            ['name' => 'Супермаркеты', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Кафе и рестораны', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Транспорт', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Хобби и развлечения', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Здоровье и красота', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Маркетплейсы', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'ЖКХ, связь, интернет', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Зачисление зарплаты', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Входящий перевод', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Иные зачисления', 'created_at' => $date, 'updated_at' => $date],
        ]);
    }
}
