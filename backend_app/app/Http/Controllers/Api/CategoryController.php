<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public function getCategories()
    {
        $categories = Category::all('name');

        $categoryNames = $categories->pluck('name')->toArray();

        return response()->json(['data' => $categoryNames], 200);
    }
}
