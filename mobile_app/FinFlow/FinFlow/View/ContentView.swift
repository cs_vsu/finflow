//
//  ContentView.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 20.03.2024.
//

import SwiftUI
import Combine
import AppMetricaCore

struct ContentView: View {
    @StateObject private var viewModel = LoginViewModel()

    var body: some View {
        NavigationView {
            ScrollView(showsIndicators: false) {
                Image("RegScreen")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 383, height: 206)
                
                Text("Рады вас видеть!")
                    .font(.system(size: 29, weight: .bold))
                    .padding(.top, 25)
                
                TextField("Введите email", text: $viewModel.email)
                    .padding(.leading, 44)
                    .frame(height: 50)
                    .background(
                        RoundedRectangle(cornerRadius: 10)
                            .fill(Color(white: 0.8, opacity: 0.2))
                    )
                    .overlay(
                        HStack {
                            Spacer()
                            Image(systemName: "envelope")
                                .foregroundColor(.gray)
                                .padding(.trailing, 22)
                        }
                    )
                    .padding(.top, 22)
                
                SecureField("Пароль", text: $viewModel.password)
                    .padding(.leading, 44)
                    .frame(height: 50)
                    .background(
                        RoundedRectangle(cornerRadius: 10)
                            .fill(Color(white: 0.8, opacity: 0.2))
                    )
                    .overlay(
                        HStack {
                            Spacer()
                            Image(systemName: "lock")
                                .foregroundColor(.gray)
                                .padding(.trailing, 22)
                        }
                    )
                    .padding(.top, 24)
                
                HStack {
                    Button(action: { viewModel.rememberMe.toggle() }) {
                        HStack {
                            Image(systemName: viewModel.rememberMe ? "checkmark.square" : "square")
                                .foregroundColor(Color(white: 0.8))
                            Text("Запомнить")
                                .font(.system(size: 13, weight: .bold))
                                .foregroundColor(Color(white: 0.8))
                        }
                    }
                    Spacer()
                    Text("Забыли пароль?")
                        .font(.system(size: 13))
                        .foregroundColor(Color.green)
                }
                .padding(.horizontal, 65)
                .padding(.top, 25)
                
                Button(action: viewModel.login) {
                    HStack {
                        Spacer()
                        Text("Продолжить")
                            .foregroundColor(.white)
                            .font(.system(size: 20, weight: .semibold))
                        Spacer()
                        Image(systemName: "arrow.right")
                            .foregroundColor(.white)
                    }
                    .background(NavigationLink("", destination: MainScreenView(), isActive: $viewModel.shouldNavigate))
                    .padding()
                    .background(Color("Button"))
                    .cornerRadius(10)
                    .padding(.top, 25)
                }
                
                if let errorMessage = viewModel.errorMessage {
                    Text(errorMessage)
                        .foregroundColor(.red)
                        .padding(.top, 10)
                }
                
                HStack {
                    Text("Новый пользователь?")
                        .font(.system(size: 15))
                    NavigationLink(destination: RegistrationView()) {
                        Text("Зарегистрироваться")
                            .font(.system(size: 15, weight: .bold))
                    }
                    .foregroundColor(Color.button)
                    .controlSize(.regular)
                    .offset(y: -2)
                }
                .padding(.top, 25)
                
                Spacer()
            }
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarHidden(true)
            .padding(.horizontal, 22)
            .background(Color.white)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
