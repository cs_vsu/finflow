//
//  FinFlowApp.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 21.03.2024.
//

import SwiftUI
import Firebase

@main
struct FinFlowApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @StateObject var registrationViewModel = RegistrationViewModel()
    @StateObject var profileViewModel = ProfileViewModel()
    
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(BudgetData())
                .environmentObject(registrationViewModel)
                .environmentObject(profileViewModel)
                .environmentObject(DataManager.shared) // Use shared instance
        }
    }
}


