import SwiftUI
import Combine



class RegistrationViewModel: ObservableObject {
    @Published var firstName: String = ""
    @Published var lastName: String = ""
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var isTermsAccepted: Bool = false
    @Published var shouldNavigate: Bool = false

    private var cancellables = Set<AnyCancellable>()

    func register() {
        guard let url = URL(string: "https://fin-flow-53e5c9805dd2.herokuapp.com/api/auth/register") else {
            print("Invalid URL")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        let body: [String: AnyHashable] = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "password": password
        ]

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
        } catch {
            print("Failed to encode JSON: \(error)")
            return
        }

        // Логирование тела запроса
        if let bodyData = request.httpBody, let bodyString = String(data: bodyData, encoding: .utf8) {
            print("Request Body: \(bodyString)")
        }

        URLSession.shared.dataTaskPublisher(for: request)
            .tryMap { output in
                guard let httpResponse = output.response as? HTTPURLResponse else {
                    throw URLError(.badServerResponse)
                }
                // Логирование ответа сервера
                print("HTTP Status Code: \(httpResponse.statusCode)")
                if httpResponse.statusCode == 200 {
                    return output.data
                } else {
                    // Логирование ошибки сервера
                    if let responseData = String(data: output.data, encoding: .utf8) {
                        print("Server Error Response: \(responseData)")
                    }
                    throw URLError(.init(rawValue: httpResponse.statusCode))
                }
            }
            .decode(type: RegistrationResponse.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("Registration failed with error: \(error)")
                }
            }, receiveValue: { response in
                UserDefaults.standard.set(response.token, forKey: "authToken")
                self.shouldNavigate = true
            })
            .store(in: &cancellables)
    }
}
