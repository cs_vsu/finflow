//
//  BudgetItem.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 18.06.2024.
//

import SwiftUI

struct BudgetItem: Identifiable, Codable {
    let id: UUID
    var category: String
    var spentAmount: Double
    var totalAmount: Double
}
