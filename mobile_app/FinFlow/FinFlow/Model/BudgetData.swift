//
//  BudgetData.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 30.04.2024.
//

import SwiftUI

class BudgetData: ObservableObject {
    @Published var budgets: [BudgetItem] = []
}
