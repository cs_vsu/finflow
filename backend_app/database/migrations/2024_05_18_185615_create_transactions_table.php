<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('currencyId')->constrained('currencies')->onDelete('cascade');
            $table->foreignId('walletId')->constrained('wallets')->onDelete('cascade');
            $table->foreignId('categoryId')->constrained('categories')->onDelete('cascade');
            $table->float('sum')->default(0);
            $table->boolean('type')->comment('true for income, false for expense');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
