<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName' => 'sometimes|string|max:255',
            'lastName'  => 'sometimes|string|max:255',
            'email'     => 'sometimes|string|email|max:255|unique:users,email,' . $request->user()->id,
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors()->all(),
            ], 422);
        }

        $user = $request->user();

        if ($request->has('firstName')) {
            $user->firstName = $request->firstName;
        }
        if ($request->has('lastName')) {
            $user->lastName = $request->lastName;
        }

        if ($request->has('email') && $request->email !== $user->email) {
            $user->email = $request->email;
        }

        $user->save();

        return response()->json([
            'message' => 'User updated successfully',
            'data' => $user
        ], 200);
    }

    public function checkPremiumStatus(Request $request)
    {
        $user = $request->user();

        $isPremium = $user->isPremium;

        return response()->json([
            'message' => 'Premium status fetched successfully',
            'isPremium' => $isPremium
        ], 200);
    }
}
