//
//  AnalysisView.swift
//  FinFlow
//
//  Created by Егор Ляшенко on 04.06.2024.
//

import SwiftUI
import SwiftUICharts

struct AnalysisView: View {
    var body: some View {
        Text("Hello, World!")
        PieCharts()
    }
}
struct PieCharts:View {
    var body: some View {
        VStack{
            PieChartView(data: [8,23,54,32,12,37,7,23,43], title: "Title")
        }
    }
}

#Preview {
    AnalysisView()
}
