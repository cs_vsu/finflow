import SwiftUI

struct RegistrationView: View {
    @EnvironmentObject private var viewModel: RegistrationViewModel
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var isTermsAccepted: Bool = false
    @Environment(\.presentationMode) var presentationMode // Для управления представлением
    
    var body: some View {
        VStack {
            Spacer()
                .frame(height: 75)
            
            Text("Добро пожаловать")
                .font(.system(size: 34, weight: .bold))
                .foregroundColor(Color(red: 0.145, green: 0.145, blue: 0.145))
                .offset(y:55)
            
            Text("Создайте новый аккаунт")
                .font(.system(size: 14, weight: .light))
                .foregroundColor(Color(red: 0.145, green: 0.145, blue: 0.145))
                .padding(.top, 45)
            
            TextField("Имя", text: $firstName)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal, 51)
                .padding(.top, 36)
            
            TextField("Фамилия", text: $lastName)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal, 51)
                .padding(.top, 22)
            
            TextField("Email", text: $email)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal, 51)
                .padding(.top, 22)
            
            SecureField("Пароль", text: $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal, 51)
                .padding(.top, 22)
            
            Toggle(isOn: $isTermsAccepted) {
                HStack {
                    Text("Я согласен на обработку данных")
                        .font(.system(size: 25))
                        .foregroundColor(Color(red: 0.145, green: 0.145, blue: 0.145))
                        .lineLimit(1)
                        .minimumScaleFactor(0.5)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                .padding(.horizontal)
            }
            .toggleStyle(CheckboxToggleStyle())
            .padding(.horizontal, 51)
            .padding(.top, 11)
            
            Button(action: {
                viewModel.firstName = firstName
                viewModel.lastName = lastName
                viewModel.email = email
                viewModel.password = password
                viewModel.isTermsAccepted = isTermsAccepted
                viewModel.register()
            }) {
                HStack {
                    Spacer()
                    Text("Продолжить")
                        .foregroundColor(.white)
                        .font(.system(size: 20, weight: .semibold))
                    Spacer()
                    Image(systemName: "arrow.right")
                        .foregroundColor(.white)
                        .offset(x: -10)
                }
            }
            .frame(width: 300, height: 50)
            .background(Color(red: 0.235, green: 0.651, blue: 0.459))
            .cornerRadius(10)
            .padding(.top, 251)
            
            HStack {
                Text("Уже зарегистрированы?")
                    .font(.system(size: 17, weight: .medium))
                    .foregroundColor(Color(red: 0.145, green: 0.145, blue: 0.145))
                    .offset(y: -25)
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Войти")
                        .font(.system(size: 17, weight: .bold))
                        .foregroundColor(Color(red: 0.235, green: 0.651, blue: 0.459))
                        .offset(y: -25)
                        .controlSize(.regular)

                }
                .background(NavigationLink("", destination: MainScreenView(), isActive: $viewModel.shouldNavigate))
            }
            .padding(.top, 50)
            
            Spacer()
        }
        .background(Color.white)
        .edgesIgnoringSafeArea(.all)
    }
}

struct CheckboxToggleStyle: ToggleStyle {
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
            
            Spacer()
            
            Image(systemName: configuration.isOn ? "checkmark.square" : "square")
                .foregroundColor(configuration.isOn ? Color(red: 0.235, green: 0.651, blue: 0.459) : Color(red: 0.796, green: 0.796, blue: 0.796))
                .onTapGesture { configuration.isOn.toggle() }
        }
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
            .environmentObject(RegistrationViewModel())
    }
}
