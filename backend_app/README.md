# Серверная часть мобильного приложения "FinFlow"

Серверная часть нашего мобильного приложения "FinFlow", была успешно развернута с использованием платформы Heroku на удаленный сервер.

С помощью Heroku, мы обеспечиваем надежную и масштабируемую работу нашего приложения, что позволяет пользователям наслаждаться бесперебойным и быстрым доступом к функциональности "FinFlow".

Для подробного изучения API и тестирования доступен Swagger, который вы можете найти по следующим ссылкам:

- Приветственная веб-страница: [(Главная)](https://fin-flow-53e5c9805dd2.herokuapp.com/)
- Open-api спецификация: [(swagger)](https://fin-flow-53e5c9805dd2.herokuapp.com/swagger)

## Окружение

- Docker 25.x.x
- Docker Compose 2.24.x

## Установка

**PROJECT**
- Создать новую директорию для проекта. В консоли перейти в созданную директорию и написать:
  `git clone https://gitlab.com/cs_vsu/finflow.git`

**DOCKER**

*Локальная сборка:*
- Перейти в директорию backend_app `cd backend_app`
- Скопировать файл .env.example и переименовать в .env, настроить параметры окружения
  `cp .env.dist .env`
- Для разворачивания серверной части FinFLow, запустите установку, выполнив команду ниже:
  `make install`

*Служебное:*
- `make migrate` - Запуск миграций
- `make shell` - Подключение в контейнер с php
- `make key` - Генерация уникального ключа приложения


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
