import SwiftUI

struct ProfileView: View {
    @EnvironmentObject private var viewModel: ProfileViewModel
    @State private var isEditing = false
    @State private var editedFirstName = ""
    @State private var editedLastName = ""
    @State private var editedEmail = ""
    @State private var shouldNavigateToLogin = false

    var body: some View {
        ZStack {
            Color.white.edgesIgnoringSafeArea(.all)
            VStack(alignment: .leading) {
                HStack {
                    Spacer()
                    VStack {
                        Image("WomanPFP")
                            .resizable()
                            .scaledToFill()
                            .frame(width: 100, height: 100)
                            .clipShape(Circle())
                            .overlay(Circle().stroke(Color.green, lineWidth: 4))
                        if isEditing {
                            TextField("Имя", text: $editedFirstName)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .padding(.top, 8)
                            TextField("Фамилия", text: $editedLastName)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .padding(.top, 8)
                            TextField("Email", text: $editedEmail)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .padding(.top, 8)
                        } else {
                            Text("\(viewModel.firstName) \(viewModel.lastName)")
                                .font(.title2)
                                .fontWeight(.medium)
                            Text(viewModel.email)
                                .font(.subheadline)
                                .foregroundColor(.gray)
                        }
                        Button(action: {
                            if isEditing {
                                viewModel.updateUserProfile(newFirstName: editedFirstName, newLastName: editedLastName, newEmail: editedEmail)
                            } else {
                                editedFirstName = viewModel.firstName
                                editedLastName = viewModel.lastName
                                editedEmail = viewModel.email
                            }
                            isEditing.toggle()
                        }) {
                            Image(systemName: isEditing ? "checkmark" : "pencil")
                        }
                        .padding(.top, 4)
                    }
                    Spacer()
                }
                .padding()

                VStack(spacing: 0) {
                    SettingItem(iconName: "gearshape.fill", iconColor: Color(red: 0.235, green: 0.651, blue: 0.459), title: "Настройки", backgroundColor: Color(red: 0.933, green: 0.898, blue: 1.0))
                    SettingItem(iconName: "arrow.up.doc.fill", iconColor: Color(red: 0.235, green: 0.651, blue: 0.459), title: "Экспорт данных", backgroundColor: Color(red: 0.933, green: 0.898, blue: 1.0))
                    SettingItem(iconName: "phone.fill", iconColor: Color(red: 0.235, green: 0.651, blue: 0.459), title: "Поддержка", backgroundColor: Color(red: 0.933, green: 0.898, blue: 1.0))
                    SettingItem(iconName: "rectangle.portrait.and.arrow.right.fill", iconColor: Color(red: 0.992, green: 0.235, blue: 0.294), title: "Выйти из профиля", backgroundColor: Color(red: 1.0, green: 0.886, blue: 0.894)) {
                        viewModel.logout()
                    }
                }
                .padding(.horizontal)
            }
            .navigationBarTitle("Профиль пользователя", displayMode: .inline)
        }
        .onAppear {
            viewModel.fetchUserProfile()
        }
        .onChange(of: viewModel.isLoggedOut) { isLoggedOut in
            if isLoggedOut {
                shouldNavigateToLogin = true
            }
        }
        .fullScreenCover(isPresented: $shouldNavigateToLogin) {
            ContentView() // Замена на ваш экран входа или регистрации
        }
        .onChange(of: viewModel.firstName) { _ in
            editedFirstName = viewModel.firstName
        }
        .onChange(of: viewModel.lastName) { _ in
            editedLastName = viewModel.lastName
        }
        .onChange(of: viewModel.email) { _ in
            editedEmail = viewModel.email
        }
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape(RoundedCorner(radius: radius, corners: corners))
    }
}

struct RoundedCorner: Shape {
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
struct SettingItem: View {
    var iconName: String
    var iconColor: Color
    var title: String
    var backgroundColor: Color
    var action: () -> Void = {}

    var body: some View {
        Button(action: action) {
            HStack {
                Image(systemName: iconName)
                    .frame(width: 32, height: 32)
                    .background(backgroundColor)
                    .foregroundColor(iconColor)
                    .cornerRadius(16)

                Text(title)
                    .font(.system(size: 16, weight: .medium))
                    .foregroundColor(Color(red: 0.161, green: 0.169, blue: 0.176))

                Spacer()
            }
            .padding(.horizontal)
            .frame(height: 60)
            .background(Color.white)
            .cornerRadius(24, corners: [.topLeft, .topRight])
        }
        .buttonStyle(PlainButtonStyle())
    }
}


struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
            .environmentObject(ProfileViewModel())
    }
}
