import SwiftUI

struct TransactionView: View {
    @EnvironmentObject var dataManager: DataManager
    
    var body: some View {
        ScrollView {
            VStack(spacing: 16) {
                if !dataManager.transactions.isEmpty {
                    TransactionSectionView(sectionTitle: "Сегодня", transactions: dataManager.transactions)
                } else {
                    Text("No transactions available.")
                        .foregroundColor(.gray)
                }
            }
            .padding()
        }
        .background(Color.white)
    }
}

struct TopNavigationView: View {
    var body: some View {
        HStack {
            Button(action: {}) {
                HStack {
                    Image(systemName: "arrow.down")
                        .foregroundColor(.black)
                    Text("Месяц")
                        .foregroundColor(Color(#colorLiteral(red: 0.129, green: 0.137, blue: 0.145, alpha: 1)))
                }
                .padding(.horizontal, 12)
                .padding(.vertical, 8)
                .background(Color(#colorLiteral(red: 0.945, green: 0.945, blue: 0.980, alpha: 1)))
                .cornerRadius(40)
            }
            Spacer()
            Button(action: {}) {
                Image(systemName: "line.horizontal.3.decrease")
                    .frame(width: 32, height: 32)
                    .padding()
                    .background(Color(#colorLiteral(red: 0.945, green: 0.945, blue: 0.980, alpha: 1)))
                    .cornerRadius(8)
            }
        }
        .padding()
    }
}

struct TransactionSectionView: View {
    let sectionTitle: String
    let transactions: [TransactionModel]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(sectionTitle)
                .font(.system(size: 18, weight: .bold))
                .foregroundColor(Color(#colorLiteral(red: 0.051, green: 0.055, blue: 0.059, alpha: 1)))
            ForEach(transactions) { transaction in
                TransactionCardView(transaction: transaction)
            }
        }
    }
}

struct TransactionCardView: View {
    let transaction: TransactionModel
    let itemFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
    
    var body: some View {
        HStack {
            RoundedRectangle(cornerRadius: 16)
                .fill(Color.blue) // Replace with transaction icon background color
                .frame(width: 40, height: 40)
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.white, lineWidth: 2) // Replace with transaction icon foreground color
                        .frame(width: 30, height: 30)
                )
            VStack(alignment: .leading) {
                Text("Category: \(transaction.categoryId)") // Replace with actual category name
                    .font(.system(size: 16, weight: .medium))
                    .foregroundColor(Color(#colorLiteral(red: 0.160, green: 0.169, blue: 0.176, alpha: 1)))
                if let description = transaction.description {
                    Text(description)
                        .font(.system(size: 13, weight: .medium))
                        .foregroundColor(Color(#colorLiteral(red: 0.569, green: 0.573, blue: 0.620, alpha: 1)))
                } else {
                    Text("No description")
                        .font(.system(size: 13, weight: .medium))
                        .foregroundColor(Color(#colorLiteral(red: 0.569, green: 0.573, blue: 0.620, alpha: 1)))
                }
            }
            Spacer()
            VStack(alignment: .trailing) {
                Text(String(format: "%.2f", Double(transaction.sum) ?? 0.0))
                    .font(.system(size: 16, weight: .bold))
                    .foregroundColor(transaction.type ? .green : .red) // Green for income, red for expense

                    .font(.system(size: 13, weight: .medium))
                    .foregroundColor(Color(#colorLiteral(red: 0.569, green: 0.573, blue: 0.620, alpha: 1)))
            }
        }
        .padding()
        .background(Color(#colorLiteral(red: 0.988, green: 0.988, blue: 0.988, alpha: 1)))
        .cornerRadius(24)
    }
}

struct TransactionView_Previews: PreviewProvider {
    static var previews: some View {
        TransactionView().environmentObject(DataManager.shared)
    }
}
