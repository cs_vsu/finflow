<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function addIncome(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'walletId' => 'required|exists:wallets,id',
            'currencyId' => 'required|exists:currencies,id',
            'categoryId' => 'required|exists:categories,id',
            'sum' => 'required|numeric',
            'description' => 'nullable|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors()->all(),
            ], 422);
        }

        $user = $request->user();
        $wallet = Wallet::where('id', $request->walletId)->where('userId', $user->id)->firstOrFail();

        $currency = Currency::findOrFail($request->currencyId);
        $exchangeRate = $currency->value;

        $convertedSum = $request->sum * $exchangeRate;

        $wallet->balance += $convertedSum;
        $wallet->save();

        $transaction = new Transaction([
            'currencyId' => $request->currencyId,
            'walletId' => $wallet->id,
            'categoryId' => $request->categoryId,
            'sum' => $request->sum,
            'type' => true,
            'description' => $request->description,
        ]);
        $transaction->save();

        return response()->json(['message' => 'Income transaction added successfully', 'transaction' => $transaction], 201);
    }

    public function addExpense(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'walletId' => 'required|exists:wallets,id',
            'currencyId' => 'required|exists:currencies,id',
            'categoryId' => 'required|exists:categories,id',
            'sum' => 'required|numeric',
            'description' => 'nullable|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors()->all(),
            ], 422);
        }

        $user = $request->user();
        $wallet = Wallet::where('id', $request->walletId)->where('userId', $user->id)->firstOrFail();

        $currency = Currency::findOrFail($request->currencyId);
        $exchangeRate = $currency->value;

        $convertedSum = $request->sum * $exchangeRate;

        $wallet->balance -= $convertedSum;
        $wallet->save();

        $transaction = new Transaction([
            'currencyId' => $request->currencyId,
            'walletId' => $wallet->id,
            'categoryId' => $request->categoryId,
            'sum' => $request->sum,
            'type' => false,
            'description' => $request->description,
        ]);
        $transaction->save();

        return response()->json(['message' => 'Expense transaction added successfully', 'transaction' => $transaction], 201);
    }

    public function deleteTransaction(Request $request, $transactionId)
    {
        $user = $request->user();
        $transaction = Transaction::where('id', $transactionId)
            ->whereHas('wallet', function ($query) use ($user) {
                $query->where('userId', $user->id);
            })->firstOrFail();

        $wallet = Wallet::findOrFail($transaction->walletId);
        $currency = Currency::findOrFail($transaction->currencyId);

        $exchangeRate = $currency->value;
        $convertedSum = $transaction->sum * $exchangeRate;

        if ($transaction->type) {
            $wallet->balance -= $convertedSum;
        } else {
            $wallet->balance += $convertedSum;
        }

        $wallet->save();
        $transaction->delete();

        return response()->json(['message' => 'Transaction deleted successfully']);
    }

    public function getTransactionsByWallet(Request $request, $walletId)
    {
        $user = $request->user();

        $wallet = Wallet::where('id', $walletId)->where('userId', $user->id)->first();

        if (!$wallet) {
            return response()->json(['message' => 'Wallet not found or not owned by the user'], 404);
        }

        $transactions = Transaction::where('walletId', $wallet->id)->get();

        return response()->json($transactions);
    }

    public function getTransactionsByCategory(Request $request, $categoryId)
    {
        $user = $request->user();

        $transactions = Transaction::where('categoryId', $categoryId)
            ->whereHas('wallet', function ($query) use ($user) {
                $query->where('userId', $user->id);
            })->get();

        if ($transactions->isEmpty()) {
            return response()->json(['message' => 'No transactions found for this category or not owned by the user'], 404);
        }

        return response()->json($transactions);
    }

    public function filterTransactions(Request $request)
    {
        $user = $request->user();
        $query = Transaction::whereHas('wallet', function ($q) use ($user) {
            $q->where('userId', $user->id);
        });

        if ($request->has('type')) {
            $query->where('type', $request->type);
        }

        if ($request->has('categoryId')) {
            $query->where('categoryId', $request->categoryId);
        }

        $transactions = $query->with('currency')->get();

        $transactions = $transactions->map(function ($transaction) {
            $rate = $transaction->currency->value;
            $transaction->convertedSum = $transaction->sum * $rate;
            return $transaction;
        });

        if ($request->has('sort')) {
            if ($request->sort == 'asc' || $request->sort == 'desc') {
                $direction = $request->sort === 'asc' ? 'asc' : 'desc';
                $transactions = $transactions->sortBy('convertedSum', SORT_REGULAR, $direction === 'desc');
            } elseif ($request->sort == 'new') {
                $transactions = $transactions->sortByDesc('created_at');
            } elseif ($request->sort == 'old') {
                $transactions = $transactions->sortBy('created_at');
            }
        }

        return response()->json([
            'transactions' => $transactions->values()->all()
        ]);
    }
}
