<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CurrencyController;
use App\Http\Controllers\Api\TransactionController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\WalletController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth/register', [AuthController::class, 'register']);
Route::post('/auth/login', [AuthController::class, 'login']);
Route::post('/auth/password', [AuthController::class, 'resetPassword']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/auth/user', [AuthController::class, 'user']);
    Route::post('/auth/logout', [AuthController::class, 'logout']);
});


Route::middleware('auth:sanctum')->group(function () {
    Route::post('/user/update', [UserController::class, 'updateUser']);
    Route::get('/user/premium', [UserController::class, 'checkPremiumStatus']);
});

Route::get('/categories', [CategoryController::class, 'getCategories']);
Route::get('/currencies', [CurrencyController::class, 'getCurrencies']);


Route::middleware('auth:sanctum')->group(function () {
    Route::get('/wallet/balance', [WalletController::class, 'getBalance']);
    Route::post('/wallet/create', [WalletController::class, 'createWallet']);
    Route::get('/wallets', [WalletController::class, 'getAllWallets']);
    Route::delete('/wallets/{walletId}', [WalletController::class, 'deleteWallet']);
    Route::patch('/wallets/{walletId}', [WalletController::class, 'updateWallet']);
});


Route::middleware('auth:sanctum')->group(function () {
    Route::post('/transaction/income', [TransactionController::class, 'addIncome']);
    Route::post('/transaction/expense', [TransactionController::class, 'addExpense']);
    Route::delete('/transaction/{transactionId}', [TransactionController::class, 'deleteTransaction']);
    Route::get('/transactions/wallet/{walletId}', [TransactionController::class, 'getTransactionsByWallet']);
    Route::get('/transactions/category/{categoryId}', [TransactionController::class, 'getTransactionsByCategory']);
    Route::post('/transactions/filter', [TransactionController::class, 'filterTransactions']);
});
